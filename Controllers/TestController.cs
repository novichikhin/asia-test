﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Newtonsoft.Json;

namespace testapp.Controllers
{
    [Route("api/[Controller]")]
    public class TestController : Controller
    {
        public static bool MonitoringWork = false;

        public List<string> Api = new List<string>()
        {
            "http://alex-asia-se-t1.azurewebsites.net/api/test",
            "http://alex-asia-ne-t1.azurewebsites.net/api/test",
            "http://alex-asia-east.azurewebsites.net/api/test"
        };

        [HttpGet("list")]
        public string List()
        {
            return Api.Aggregate("", (s,s1) => s + s1 + "\n" );
        }
        
        [HttpPost]
        public TestDto TestAction([FromBody]TestDto dto)
        {
            dto.Time = DateTime.UtcNow;
            dto.Id = dto.Id + 1;
            return dto;
        }

        [HttpGet]
        public string Test()
        {
            var result = "";

            var request = new TestDto()
            {
                Id = 115,
                Text = "1234567890"
            };
            for (int i = 0; i < 100; i++) request.Text += "1234567890";


            foreach (var api in Api)
            {
                request.Id += 13;
                var message1 = DoTest(api, request);
                result += $"{api}  {message1} \n";
            }

            return result;
        }

        [HttpGet("monitor/start")]
        public string StartMonitoring()
        {
            if (!MonitoringWork)
            {
                MonitoringWork = true;
                Task.Run(DoMonitoring);
                return "monitoring started";
            }

            return "monitoring active";
        }

        [HttpGet("monitor")]
        public string ShowMonitoring()
        {
            using (var file = new StreamReader("log.txt"))
            {
                return file.ReadToEnd();
            }
        }

        private async Task DoMonitoring()
        {
            while (MonitoringWork)
            {
                Test();
                Test();
                var result = Test();
                using (var file = new StreamWriter("log.txt", true))
                {
                    file.WriteLine($"==={DateTime.UtcNow}===");
                    file.WriteLine(result);
                    file.WriteLine();
                }

                await Task.Delay(TimeSpan.FromMinutes(5));
            }
        }

        public string DoTest(string url, TestDto dto)
        {
            var sw = new Stopwatch();
            sw.Start();
            try
            {
                var res = Post<TestDto, TestDto>(url, dto);
                sw.Stop();
                if (res.Id-1 != dto.Id) return "incorect id!";
                return $"success. time {sw.ElapsedMilliseconds}ms. server time: {res.Time}";
            }
            catch (Exception ex)
            {
                return "Exception: " + ex.Message;
            }
        }

        private TRes Post<TReq, TRes>(string url, TReq req) where TReq: class where TRes: class
        {
            var json = JsonConvert.SerializeObject(req);
            var buf = Encoding.UTF8.GetBytes(json);
            
            var request = WebRequest.Create(url);
            request.Method = "POST";
            request.ContentType = "application/json";
            request.ContentLength = buf.Length;
            using (Stream stream = request.GetRequestStream())
            {
                stream.Write(buf, 0, buf.Length);
            }

            var rejson = "";
            using (WebResponse response = request.GetResponse())
            {
                using (Stream stream = response.GetResponseStream())
                {
                    using (StreamReader sr99 = new StreamReader(stream))
                    {
                        rejson = sr99.ReadToEnd();
                    }
                }
            }

            var res = JsonConvert.DeserializeObject<TRes>(rejson);
            return res;
        }
    }

    public class TestDto
    {
        public int Id { get; set; }
        public DateTime Time;
        public string Text { get; set; }
    }
}